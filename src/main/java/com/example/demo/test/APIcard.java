package com.example.demo.test;

import com.example.demo.test.entry.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


@RestController
@Api(tags = "接口")
@CrossOrigin
@RequestMapping( method = {RequestMethod.POST, RequestMethod.GET})
public class APIcard {
    @Autowired
    JdbcTemplate jdbcTemplate = new JdbcTemplate();
    @RequestMapping(value = "/")
    String index() {
        return "index";
    }
    @RequestMapping(value = "/api/getimgkinds")
    ReturnData getkinds() {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select  * from imgkinds");
        return new ReturnData("0", res);
    }
    @RequestMapping(value = "/api/goods/getdesc")
    ReturnData getgoodinfotext() {
        return new ReturnData("0", "1");
    }
    @RequestMapping(value = "/api/getimages")
    ReturnData getimages(String id) {
        List<Map<String, Object>> query= jdbcTemplate.queryForList("select * from newspic where id=?", id);
        return new ReturnData("0", query);
    }



    @RequestMapping("/api/getthumimages")
    ReturnData getthumimages(String id) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select * from imgs where fromid=?", id);
        return new ReturnData("0", res);
    }

    @RequestMapping("/api/goods/getinfo")
    ReturnData getinfo(String id) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select * from iteminfos where id=?", id);
        return new ReturnData("0", res);

    }

    @RequestMapping("/api/getgoods")
    ReturnData getgoods(String pageindex) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select * from iteminfos");
        return new ReturnData("0", res);
    }

    @RequestMapping("/api/getnewslist")
    ReturnData getnewlist() {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select id,click,add_time,zhaiyao,img_url,title from blobs");
        return new ReturnData("0", res);
    }

    @RequestMapping("/api/getnew")
    ReturnData getnew(String id) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select add_time,title,content,click from blobs");
        return new ReturnData("0", res);
    }

    @RequestMapping("/api/getlunbo")
    ReturnData getlunbo() {
        List<Map<String, Object>> ans = jdbcTemplate.queryForList("select * from lunbopic");
        return new ReturnData("0", ans);
    }

    @RequestMapping("/api/buybuybuy")
    ReturnData buybuybuy(String id) {
        List<Map<String, Object>> res1 = jdbcTemplate.queryForList("select * from buycar where itemid=?", id);
        if (res1.size() == 0) {
            int update = jdbcTemplate.update("INSERT INTO buycar(itemid, count) VALUES (?, 1)", id);
            return new ReturnData("0", update);
        }
        int update = jdbcTemplate.update("update buycar set count=count+1 where itemid=?", id);
        return new ReturnData("0", update);
    }
    @RequestMapping("/api/cutcutcut")
    ReturnData cutcutcur(String id) {
        List<Map<String, Object>> res1 = jdbcTemplate.queryForList("select * from buycar where itemid=?", id);
        if (res1.size() == 0) {
            return new ReturnData("0", 1);
        }
        int update = jdbcTemplate.update("update buycar set count=count-1 where itemid=?", id);
        return new ReturnData("0", update);
    }

    @RequestMapping("/api/getsomethingcount")
    ReturnData getsomethingcount(String id) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select count from buycar where itemid=?", id);
        return new ReturnData("0", res);
    }
    @RequestMapping("/api/getallbuycar")
    ReturnData getallbuycar(String id) {
        List<Map<String, Object>> res = jdbcTemplate.queryForList("select img_url as img,title,zhaiyao as remark,count as number,sell_price as price ,iteminfos.id from buycar,iteminfos where buycar.id=iteminfos.id AND count>0");
        return new ReturnData("0", res);
    }
    @RequestMapping(value="/file/{img}")
    File returnfile(@PathVariable String img){
        return new  File(img);
    }


    @Value("${myfile.root.path}")
    public String fileRootPath;
    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public ReturnData upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String filePath = ""; // 文件保存的位置
        String urlPath = "";// 文件web浏览路径
        Assert.isTrue(!file.isEmpty(), "文件为空");
        // 原始名 以 a.jpg为例
        String originalFilename = file.getOriginalFilename();
        // 获取后缀并拼接'/'用于分类,也可以用日期 例: suffix = "jpg/"
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1) + "/";
        // 加上时间戳生成新的文件名,防止重复 newFileName = "1595511980146a.jpg"
        String newFileName = System.currentTimeMillis() + originalFilename;
        filePath = fileRootPath + suffix + newFileName;

        try {
            File file1 = new File(filePath);
            if (!file1.exists()) {
                file1.mkdirs(); // 要是目录不存在,创建一个
            }
            file.transferTo(file1);              // 保存起来
            urlPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/upload/" + suffix + newFileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(filePath+"转换后:"+urlPath);
        return new ReturnData("0",urlPath);
    }



}





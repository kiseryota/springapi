package com.example.demo.test;

import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import springfox.documentation.service.ResponseMessage;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
@Slf4j
@CrossOrigin
@ServerEndpoint(value = "/ws")
@Component
public class websocketcard {
    private static AtomicInteger onlineCount = new AtomicInteger(0);
    private static JdbcTemplate jdbcTemplate=new JdbcTemplate(new DriverManagerDataSource("jdbc:mysql://127.0.0.1:3306/test","root","123456"));
    private static List<Map<String,Object>> rs=jdbcTemplate.queryForList("select * from chat");
    private static Map<String, Session> clients = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session) {
        onlineCount.incrementAndGet(); // 在线数加1
        clients.put(session.getId(), session);

        log.info("有新连接加入：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }


    @OnClose
    public void onClose(Session session) {
        onlineCount.decrementAndGet(); // 在线数减1
        clients.remove(session.getId());
        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }

    @OnMessage
    public void onmessage(String message, Session session) throws Exception {
        int add = message.indexOf("add");
        int rep=message.indexOf("rep");
        if(add!=-1){
            String[] adds = message.split(" ");
            if(adds.length==3){
                int update = jdbcTemplate.update("insert into chat value (?,?)", adds[1], adds[2]);
                if(update>0){
                    session.getBasicRemote().sendText("添加成功");
                }else{
                    session.getBasicRemote().sendText("添加失败 已有重复doc 请使用rep指令或者联系管理员");
                }
                return;
            }else if (adds.length!=1){
                session.getBasicRemote().sendText("用法:add Q A");
                return;
            }
            return;
        }
        if(rep!=-1){
            String[] adds = message.split(" ");
            if(adds.length==3){
                int update = jdbcTemplate.update("update chat set ans=? where doc=?", adds[2], adds[1]);
                if(update==0){
                    session.getBasicRemote().sendText("修改失败 请使用add指令添加");
                }
                else{
                    session.getBasicRemote().sendText("添加成功");
                }
                return;
            }else if (adds.length!=1){
                session.getBasicRemote().sendText("用法:add Q A");
                return;
            }
            return;
        }
        session.getBasicRemote().sendText(judge(message));




    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }
    private String judge(String doc) throws Exception {

        rs=jdbcTemplate.queryForList("select * from chat");
        int sz=rs.size();
        int index=0;
        double r=0;
        for(int i=0;i<sz;i++){
            double doc1 = getSimilarity2(rs.get(i).get("doc").toString(), doc);
            if(doc1>r){
                r=doc1;
                index=i;
            }
        }
        log.info("{}的结果为{}",doc,r);
        if(r<0.25){
            return "营业时间为 24:00-25:00 非时间点nores";
        }
        return rs.get(index).get("ans").toString();
    }

    private void sendMessage(String message, Session toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息[{}]", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败：{}", e);
        }
    }
    private static double getSimilarity(String doc1, String doc2) {
        if (doc1.equals("")|| doc2.equals("")) {
            return 0L;
        }
        Map<Character,int[]> algMap=new HashMap<>(10);
        for (int i = 0; i<doc1.length(); i++) {
            char d1 = doc1.charAt(i);
            int[] fq = algMap.get(d1);
            if (fq != null && fq.length == 2) {
                fq[0]++;
            } else {
                fq = new int[2];
                fq[0] = 1;
                fq[1] = 0;
                algMap.put(d1, fq);
            }
        }
        for (int i = 0; i<doc2.length(); i++) {
            char d2 = doc2.charAt(i);
            int[] fq = algMap.get(d2);
            if (fq != null && fq.length == 2) {
                fq[1]++;
            } else {
                fq = new int[2];
                fq[0] = 0;
                fq[1] = 1;
                algMap.put(d2, fq);
            }
        }
        double sqdoc1 = 0;
        double sqdoc2 = 0;
        double denuminator = 0;
        for (Map.Entry entry : algMap.entrySet()) {
            int[] c = (int[]) entry.getValue();
            denuminator += c[0] * c[1];
            sqdoc1 += c[0] * c[0];
            sqdoc2 += c[1] * c[1];
        }
        return denuminator / Math.sqrt(sqdoc1 * sqdoc2);
    }
    public static Vector<String> participle( String str ) {

        Vector<String> str1 = new Vector<String>() ;//对输入进行分词

        try {

            StringReader reader = new StringReader( str );
            IKSegmenter ik = new IKSegmenter(reader,true);//当为true时，分词器进行最大词长切分
            Lexeme lexeme = null ;

            while( ( lexeme = ik.next() ) != null ) {
                str1.add( lexeme.getLexemeText() );
            }

            if( str1.size() == 0 ) {
                return null ;
            }

            //分词后
            System.out.println( "str分词后：" + str1 );

        } catch ( IOException e1 ) {
            System.out.println();
        }
        return str1;
    }
    public static double getSimilarity2(String d1, String d2) throws Exception {
        Vector<String> T1 = participle(d1),T2=participle(d2);
        int size = 0 , size2 = 0 ;
        if ( T1 != null && ( size = T1.size() ) > 0 && T2 != null && ( size2 = T2.size() ) > 0 ) {

            Map<String, double[]> T = new HashMap<String, double[]>();

            //T1和T2的并集T
            String index = null ;
            for ( int i = 0 ; i < size ; i++ ) {
                index = T1.get(i) ;
                if( index != null){

                    double[] c = new double[2];
                    c[0] = 1;	//T1的语义分数Ci
                    c[1] = 0.1;//T2的语义分数Ci
                    T.put( index, c );
                }
            }

            for ( int i = 0; i < size2 ; i++ ) {
                index = T2.get(i) ;
                if( index != null ){
                    double[] c = T.get( index );
                    if( c != null && c.length == 2 ){
                        c[1] = 1; //T2中也存在，T2的语义分数=1
                    }else {
                        c = new double[2];
                        c[0] = 0.1; //T1的语义分数Ci
                        c[1] = 1; //T2的语义分数Ci
                        T.put( index , c );
                    }
                }
            }

            //开始计算，百分比
            Iterator<String> it = T.keySet().iterator();
            double s1 = 0 , s2 = 0, Ssum = 0;  //S1、S2
            while( it.hasNext() ){
                double[] c = T.get( it.next() );
                Ssum += c[0]*c[1];
                s1 += c[0]*c[0];
                s2 += c[1]*c[1];
            }
            //百分比
            return Ssum / Math.sqrt( s1*s2 );
        } else {
            throw new Exception("传入参数有问题！");
        }
    }
}

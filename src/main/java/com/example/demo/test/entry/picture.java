package com.example.demo.test.entry;

import lombok.Data;

@Data
public class picture {
    int id;
    String url;//网页
    String img;//图片路径
    String title;
    String zhaiyao;

    public picture(int id, String url, String img, String title, String zhaiyao) {
        this.id = id;
        this.url = url;
        this.img = img;
        this.title = title;
        this.zhaiyao = zhaiyao;
    }

    public picture(int id, String url) {
        this.id = id;
        this.url = url;
    }
}

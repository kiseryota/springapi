package com.example.demo.test.entry;

import lombok.*;

import java.util.List;

@Data
public class ReturnData {
    public String status;
    public Object message;

    public ReturnData(String status, Object message) {
        this.status = status;
        this.message = message;
    }
}

package com.example.demo.test.entry;

import lombok.Data;

@Data
public class news {
    int id,click;
    String title,add_time,zhaiyao,img_url,content;

    public news(int id, int click, String title, String add_time, String zhaiyao, String img_url, String content) {
        this.id = id;
        this.click = click;
        this.title = title;
        this.add_time = add_time;
        this.zhaiyao = zhaiyao;
        this.img_url = img_url;
        this.content = content;
    }

}

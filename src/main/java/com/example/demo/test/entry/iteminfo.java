package com.example.demo.test.entry;

public class iteminfo {
    int id;
    int click;

    public iteminfo(String tit,int id, int click, int sell_price, int market_price, int stock_quantity, String addtime, String zhaiyao, String imgurl) {
        this.title=tit;
        this.id = id;
        this.click = click;
        this.sell_price = sell_price;
        this.market_price = market_price;
        this.stock_quantity = stock_quantity;

        this.addtime = addtime;
        this.zhaiyao = zhaiyao;
        this.img_url = imgurl;
    }

    int sell_price;
    int market_price;
    int stock_quantity;
    String addtime,zhaiyao,img_url,title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public int getSell_price() {
        return sell_price;
    }

    public void setSell_price(int sell_price) {
        this.sell_price = sell_price;
    }

    public int getMarket_price() {
        return market_price;
    }

    public void setMarket_price(int market_price) {
        this.market_price = market_price;
    }

    public int getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(int stock_quantity) {
        this.stock_quantity = stock_quantity;
    }


    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getZhaiyao() {
        return zhaiyao;
    }

    public void setZhaiyao(String zhaiyao) {
        this.zhaiyao = zhaiyao;
    }

    public String getImgurl() {
        return img_url;
    }

    public void setImgurl(String imgurl) {
        this.img_url = imgurl;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", click=" + click +
                ", sell_price=" + sell_price +
                ", title=" + title +
                ", market_price=" + market_price +
                ", stock_quantity=" + stock_quantity +
                ", addtime='" + addtime + '\'' +
                ", zhaiyao='" + zhaiyao + '\'' +
                ", imgurl='" + img_url + '\'' +
                '}';
    }
}

package com.example.demo.test.sqldocutil;

import lombok.Data;

@Data
public class TableInfo {

    private String tableName;
    private String tableComment;

}

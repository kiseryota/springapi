package com.example.demo.test.sqldocutil;

import java.util.List;


public interface TableService {

    List<TableInfo> getTableList();

    List<FieldInfo> getFieldInfoList(String tableName);
}

package com.example.demo.test.sqldocutil;

import lombok.Data;

@Data
public class FieldInfo {
    private String fieldName;
    private String defaultValue;
    private String isEmpty;
    private String fieldType;
    private String fieldExplain;
    private String fieldDetails;

}

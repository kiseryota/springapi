package com.example.demo.test.config;


import com.example.demo.test.entry.ReturnData;
import io.swagger.annotations.Contact;
import org.assertj.core.internal.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class pro {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
              .paths(PathSelectors.regex("^((?!/[e].*).)*"))
                .build().apiInfo(new ApiInfoBuilder()
                        .title("Swagger")
                        .description("Swagger")
                        .version("9.0")
                        .license("License")

                        .build());
    }
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
